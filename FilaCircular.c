#include <stdio.h>
#include "FilaCircular.h"

void novaFila(PFC f) {
    f->tam = 0;
    f->inicio = f->fim = -1;
}

int filaVazia(FC *f) {
    return (f->tam == 0) ? 1 : 0;
}

int filaCheia(FC *f) {
    return (f->tam == MAX_FILA) ? 1 : 0;
}

int inserir(FC *f, int elemento) {
    if (filaCheia(f)) return 0;
    
    f->fim = (f->fim+1) % MAX_FILA;
    f->fila[f->fim] = elemento;
    f->tam++;
    return 1;
}

int removerFila(FC *f, int *el) {
    if (filaVazia(f)) return 0;
    
    *el = f->fila[f->inicio];
    f->tam--;
    f->inicio = (f->inicio+1) % MAX_FILA;
    return 1;
}

int get(FC *f, int posicao, int *el) {
    if (posicao > f->fim) return 0;
    
    *el = f->fila[(f->inicio + posicao) % MAX_FILA];
    return 1;
}
