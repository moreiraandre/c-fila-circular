#include <stdio.h>
#include "FilaCircular.h"

FC Fila;

int main() {
    novaFila(&Fila);
    int x = 0;
    printf("INSERINDO INTEIROS ATÉ ENCHER A FILA\n");
    while (inserir(&Fila, ++x) == 1);
        
    printf("TAMANHO DA FILA: %i\n", Fila.tam);
        
    int el;
    printf("REMOVENDO ELEMENTOS ATÉ A FILA FICAR VAZIA\n");
    while (removerFila(&Fila, &el) == 1);
    
    printf("FIM.\n");
    return 0;
}
