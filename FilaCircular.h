#define MAX_FILA 100

typedef struct {
    int fila[MAX_FILA];
    int tam, inicio, fim;
} FC; // FILA CIRCULAR

typedef FC *PFC;

void novaFila(PFC);

int filaVazia(PFC);

int filaCheia(PFC);

int inserir(PFC, int elemento);

int removerFila(PFC, int *el);

int get(PFC, int posicao, int *el);
